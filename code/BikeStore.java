//Chiril Barba 2232864
public class BikeStore {
  public static void main(String[] args){
    Bicycle[] bikes = new Bicycle[4];
    bikes[0] = new Bicycle("Tesla",21,40);
    bikes[1] = new Bicycle("Speed",45,60);
    bikes[2] = new Bicycle("Toaster",6,100);
    bikes[3] = new Bicycle("Pizza",36,10);
    for(int i = 0; i<bikes.length; i++){
      System.out.println(bikes[i]);
    }
  }  
}
